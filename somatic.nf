#!/usr/bin/env nextflow

include { manifest_to_raw_fqs } from '../preproc/preproc.nf'
include { raw_fqs_to_procd_fqs } from '../preproc/preproc.nf'

include { procd_fqs_to_alns } from '../alignment/alignment.nf'

// Strelka2
include { strelka2_somatic } from '../strelka2/strelka2.nf'

// MuTect2
include { gatk_mutect2_matched } from '../gatk4/gatk4.nf'
include { gatk_learn_read_orientation_model } from '../gatk4/gatk4.nf'
include { gatk_index_feature_file } from '../gatk4/gatk4.nf'
include { gatk_get_pileup_summaries } from '../gatk4/gatk4.nf'
include { gatk_calculate_contamination } from '../gatk4/gatk4.nf'
include { gatk_filter_mutect_calls } from '../gatk4/gatk4.nf'

// Abra2
include { abra2_cadabra } from '../abra2/abra2.nf'

// VarScan2
include { samtools_mpileup } from '../samtools/samtools.nf'
include { samtools_mpileup_parallel } from '../samtools/samtools.nf'
include { varscan2_somatic } from '../varscan2/varscan2.nf'
include { varscan2_somatic_parallel } from '../varscan2/varscan2.nf'

// Misc
include { samtools_index } from '../samtools/samtools.nf'
include { samtools_faidx } from '../samtools/samtools.nf'
include { htslib_bgzip_ref } from '../htslib/htslib.nf'
include { htslib_tabix } from '../htslib/htslib.nf'
include { htslib_tabix as htslib_tabix_pon } from '../htslib/htslib.nf'
include { htslib_tabix as htslib_tabix_af } from '../htslib/htslib.nf'
include { make_ancillary_index_files } from '../alignment/alignment.nf'

// Variant filtering
include { bcftools_filter as bcftools_filter_mutect2 } from '../bcftools/bcftools.nf'
include { bcftools_filter as bcftools_filter_strelka2 } from '../bcftools/bcftools.nf'
include { bcftools_filter as bcftools_filter_abra2 } from '../bcftools/bcftools.nf'
include { bcftools_filter as bcftools_filter_varscan2 } from '../bcftools/bcftools.nf'

// Variant normalization
include { bcftools_norm as bcftools_norm_mutect2 } from '../bcftools/bcftools.nf'
include { bcftools_norm as bcftools_norm_stelka2 } from '../bcftools/bcftools.nf'
include { bcftools_norm as bcftools_norm_abra2 } from '../bcftools/bcftools.nf'

// Variant intersection
include { bedtools_intersect_vcfs } from '../bedtools/bedtools.nf'

// Variant union
include { jacquard_merge } from '../jacquard/jacquard.nf'

// Cleaning
include { clean_work_dirs as clean_pileups } from '../utilities/utilities.nf'

workflow manifest_to_som_vars {
// require:
//   MANIFEST
//   params.somatic$manifest_to_som_vars$fq_trim_tool
//   params.somatic$manifest_to_som_vars$fq_trim_tool_parameters
//   params.somatic$manifest_to_som_vars$aln_tool
//   params.somatic$manifest_to_som_vars$aln_tool_parameters
//   params.somatic$manifest_to_som_vars$som_var_caller
//   params.somatic$manifest_to_som_vars$som_var_caller_parameters
//   params.somatic$manifest_to_som_vars$som_var_caller_suffix
//   params.somatic$manifest_to_som_vars$aln_ref
//   params.somatic$manifest_to_som_vars$bed
//   params.somatic$manifest_to_som_vars$som_var_pon_vcf
//   params.somatic$manifest_to_som_vars$som_var_af_vcf
//   params.somatic$manifest_to_som_vars$known_sites_ref
//   params.somatic$manifest_to_som_vars$species
  take:
    manifest
    fq_trim_tool
    fq_trim_tool_parameters
    aln_tool
    aln_tool_parameters
    som_var_caller
    som_var_caller_parameters
    som_var_caller_suffix
    aln_ref
    bed
    som_var_pon_vcf
    som_var_af_vcf
    known_sites_ref
    species
  main:
    manifest_to_raw_fqs(
      manifest)
    raw_fqs_to_som_vars(
      manifest_to_raw_fqs.out.fqs,
      fq_trim_tool,
      fq_trim_tool_parameters,
      aln_tool,
      aln_tool_parameters,
      som_var_caller,
      som_var_caller_parameters,
      som_var_caller_suffix,
      aln_ref,
      bed,
      som_var_pon_vcf,
      som_var_af_vcf,
      known_sites_ref,
      species,
      manifest)
  emit:
    som_vars = raw_fqs_to_som_vars.out.som_vars
}

workflow raw_fqs_to_som_vars {
// require:
//   FQS
//   params.somatic$raw_fqs_to_som_vars$fq_trim_tool
//   params.somatic$raw_fqs_to_som_vars$fq_trim_tool_parameters
//   params.somatic$raw_fqs_to_som_vars$aln_tool
//   params.somatic$raw_fqs_to_som_vars$aln_tool_parameters
//   params.somatic$raw_fqs_to_som_vars$som_var_caller
//   params.somatic$raw_fqs_to_som_vars$som_var_caller_parameters
//   params.somatic$raw_fqs_to_som_vars$som_var_caller_suffix
//   params.somatic$raw_fqs_to_som_vars$aln_ref
//   params.somatic$raw_fqs_to_som_vars$bed
//   params.somatic$raw_fqs_to_som_vars$som_var_pon_vcf
//   params.somatic$raw_fqs_to_som_vars$som_var_af_vcf
//   params.somatic$raw_fqs_to_som_vars$known_sites_ref
//   params.somatic$raw_fqs_to_som_vars$species
//   MANIFEST
  take:
    fqs
    fq_trim_tool
    fq_trim_tool_parameters
    aln_tool
    aln_tool_parameters
    som_var_caller
    som_var_caller_parameters
    som_var_caller_suffix
    aln_ref
    bed
    som_var_pon_vcf
    som_var_af_vcf
    known_sites_ref
    species
    manifest
  main:
    raw_fqs_to_procd_fqs(
      fqs,
      fq_trim_tool,
      fq_trim_tool_parameters)
    procd_fqs_to_som_vars(
      raw_fqs_to_procd_fqs.out.procd_fqs,
      aln_tool,
      aln_tool_parameters,
      som_var_caller,
      som_var_caller_parameters,
      som_var_caller_suffix,
      aln_ref,
      bed,
      som_var_pon_vcf,
      som_var_af_vcf,
      known_sites_ref,
      species,
      manifest)
  emit:
    som_vars = procd_fqs_to_som_vars.out.som_vars
}

workflow procd_fqs_to_som_vars {
// require:
//   PROCD_FQS
//   params.somatic$raw_fqs_to_som_vars$aln_tool
//   params.somatic$raw_fqs_to_som_vars$aln_tool_parameters
//   params.somatic$raw_fqs_to_som_vars$som_var_caller
//   params.somatic$raw_fqs_to_som_vars$som_var_caller_parameters
//   params.somatic$raw_fqs_to_som_vars$som_var_caller_suffix
//   params.somatic$raw_fqs_to_som_vars$aln_ref
//   params.somatic$raw_fqs_to_som_vars$bed
//   params.somatic$raw_fqs_to_som_vars$som_var_pon_vcf
//   params.somatic$raw_fqs_to_som_vars$som_var_af_vcf
//   params.somatic$raw_fqs_to_som_vars$known_sites_ref
//   params.somatic$raw_fqs_to_som_vars$species
//   MANIFEST
  take:
    procd_fqs
    aln_tool
    aln_tool_parameters
    som_var_caller
    som_var_caller_parameters
    som_var_caller_suffix
    aln_ref
    bed
    som_var_pon_vcf
    som_var_af_vcf
    known_sites_ref
    species
    manifest
  main:
    procd_fqs_to_alns(
      procd_fqs,
      aln_tool,
      aln_tool_parameters,
      aln_ref,
      params.dummy_file,
      params.dummy_file)
    alns_to_som_vars(
      procd_fqs_to_alns.out.alns,
      som_var_caller,
      som_var_caller_parameters,
      som_var_caller_suffix,
      aln_ref,
      bed,
      som_var_pon_vcf,
      som_var_af_vcf,
      known_sites_ref,
      species,
      manifest)
  emit:
    som_vars = alns_to_som_vars.out.som_vars
}

workflow alns_to_som_vars {
// require:
//   ALNS
//   params.somatic$manifest_to_som_vars$fq_trim_tool
//   params.somatic$manifest_to_som_vars$fq_trim_tool_parameters
//   params.somatic$manifest_to_som_vars$aln_tool
//   params.somatic$manifest_to_som_vars$aln_tool_parameters
//   params.somatic$manifest_to_som_vars$som_var_caller
//   params.somatic$manifest_to_som_vars$som_var_caller_parameters
//   params.somatic$manifest_to_som_vars$som_var_caller_suffix
//   params.somatic$manifest_to_som_vars$aln_ref
//   params.somatic$manifest_to_som_vars$bed
//   params.somatic$manifest_to_som_vars$som_var_pon_vcf
//   params.somatic$manifest_to_som_vars$som_var_af_vcf
//   params.somatic$manifest_to_som_vars$known_sites_ref
//   params.somatic$manifest_to_som_vars$species
  take:
    alns
    som_var_caller
    som_var_caller_parameters
    som_var_caller_suffix
    aln_ref
    bed
    som_var_pon_vcf
    som_var_af_vcf
    known_sites_ref
    species
    manifest
  main:
    som_vars = Channel.empty()
    mutect2_vars = Channel.empty()
    strelka2_snv_vars = Channel.empty()
    strelka2_indel_vars = Channel.empty()
    mutect2_vars = Channel.empty()
    abra2_vars = Channel.empty()
    varscan2_snv_vars = Channel.empty()
    varscan2_indel_vars = Channel.empty()
    // Indexing and combining tumor/normal patient samples.
    samtools_index(
      alns,
      '')
    manifest.filter{ it[5] =~ /TRUE/ }.set{ norms }
    manifest.filter{ it[5] =~ /FALSE/ }.set{ tumors }
    alns
      .join(samtools_index.out.bais, by: [0, 1])
      .set{ bams_bais }
    bams_bais
      .join(norms, by: [0, 1])
      .map{ [it[0], it[1], it[2], it[3], it[5]] }
      .set{ norm_bams_bais }
    bams_bais
      .join(tumors, by: [0, 1])
      .map{ [it[0], it[1], it[2], it[3], it[5]] }
      .set{ tumor_bams_bais }
    norm_bams_bais
      .join(tumor_bams_bais, by: [0, 2])
      .set{ norm_tumor_bams_bais }
    // Indexing BED file
    htslib_bgzip_ref(
      bed)
    htslib_tabix(
      htslib_bgzip_ref.out.bgzip_files)
    make_ancillary_index_files(
      aln_ref)

    if( som_var_caller =~ /,/ ) {
      println "Running multiple somatic variant callers -- use tool-specific output channels."
      multitools = 'True'
    }

    som_var_caller_parameters = Eval.me(som_var_caller_parameters)
    som_var_caller_suffix = Eval.me(som_var_caller_suffix)

    if( som_var_caller =~ /varscan2/ ) {
      // VarScan2 is the only currently supported variant caller that requires
      // pileup files as inputs. These are computationally expensive to
      // generate and shouldn't be generated unless required. As such,
      // samtools_mpileup is being scoped within the VarScan2 condition.
      // This should be modified to be run samstools_mpileup by chromosome in
      // parallel instead of serially.
      samtools_faidx(
        aln_ref,
        '')
      samtools_mpileup_parallel(
        bams_bais.map{ [it[0], it[1], it[2], it[3], it[5]] },
        samtools_faidx.out.faidx_file,
        '')
      samtools_mpileup_parallel.out.pileups
        .join(norms, by: [0, 1])
        .map{ [it[0], it[1], it[2], it[3]] }
        .set{ norm_pileups }
      samtools_mpileup_parallel.out.pileups
        .join(tumors, by: [0, 1])
        .map{ [it[0], it[1], it[2], it[3]]  }
        .set{ tumor_pileups }
      norm_pileups
        .join(tumor_pileups, by: [0, 2])
        .set{ norm_tumor_pileups }

      varscan2_parameters = som_var_caller_parameters['varscan2'] ? som_var_caller_parameters['varscan2'] : ''
      varscan2_suffix = som_var_caller_suffix['varscan2'] ? som_var_caller_suffix['varscan2'] : ''
      varscan2_somatic_parallel(
        norm_tumor_pileups,
        bed,
        varscan2_parameters,
        varscan2_suffix,
        species)
      varscan2_somatic_parallel.out.snv_vcfs
        .set{ varscan2_snv_vars }
      varscan2_somatic_parallel.out.indel_vcfs
        .set{ varscan2_indel_vars }
      varscan2_somatic_parallel.out.snv_vcfs
        .concat(varscan2_somatic_parallel.out.indel_vcfs)
        .concat(norm_tumor_pileups)
        .groupTuple(by:0, size: 1)
        .flatten()
        .filter{ it =~ /.pileups$/ }
        .set{ pileups_done_signal }
      clean_pileups(
          pileups_done_signal)

    }
    if( som_var_caller =~ /strelka2/ ) {
      strelka2_parameters = som_var_caller_parameters['strelka2'] ? som_var_caller_parameters['strelka2'] : ''
      strelka2_suffix = som_var_caller_suffix['strelka2'] ? som_var_caller_suffix['strelka2'] : ''
      strelka2_somatic(
        norm_tumor_bams_bais,
        make_ancillary_index_files.out.collective_idx_files,
        htslib_tabix.out.tabix_file,
        strelka2_parameters,
        strelka2_suffix)
      strelka2_somatic.out.snv_vcfs
        .set{ strelka2_snv_vars }
      strelka2_somatic.out.indel_vcfs
        .set{ strelka2_indel_vars }
    }
    if( som_var_caller =~ /mutect2/ ) {
      mutect2_parameters = som_var_caller_parameters['mutect2'] ? som_var_caller_parameters['mutect2'] : ''
      gatk_learn_read_orientation_model_parameters = som_var_caller_parameters['gatk_learn_read_orientation_model'] ? som_var_caller_parameters['gatk_learn_read_orientation_model'] : ''
      gatk_index_feature_file_parameters = som_var_caller_parameters['gatk_index_feature_file'] ? som_var_caller_parameters['gatk_index_feature_file'] : ''
      gatk_get_pileup_summaries_parameters = som_var_caller_parameters['gatk_get_pileup_summaries'] ? som_var_caller_parameters['gatk_get_pileup_summaries'] : ''
      mutect2_suffix = som_var_caller_suffix['mutect2'] ? som_var_caller_suffix['mutect2'] : ''
      gatk_filter_mutect_calls_suffix = som_var_caller_suffix['gatk_filter_mutect_calls_suffix'] ? som_var_caller_suffix['gatk_filter_mutect_calls_suffix'] : '.gfilt'
      gatk_filter_mutect_calls_parameters = som_var_caller_suffix['gatk_filter_mutect_calls_suffix'] ? som_var_caller_suffix['gatk_filter_mutect_calls_parameters'] : ''
      htslib_tabix_pon(
        som_var_pon_vcf)
      htslib_tabix_af(
        som_var_af_vcf)
      gatk_mutect2_matched(
        norm_tumor_bams_bais,
        make_ancillary_index_files.out.collective_idx_files,
        bed,
        mutect2_parameters,
        mutect2_suffix,
        htslib_tabix_pon.out.tabix_file,
        htslib_tabix_af.out.tabix_file,
        species)
      if( species =~ /[Hh]uman|hs|HS|[Hh]omo/ ) {
          chrom_count = 25
      } else if( species =~ /[Mm]ouse|mm|MM|[Mm]us/ ) {
          chrom_count = 22
      }  // Keep RAFT happy
      gatk_learn_read_orientation_model(
        gatk_mutect2_matched.out.f1r2_tar_gzs.groupTuple(by: [0,1,2,3], size:chrom_count),
        gatk_learn_read_orientation_model_parameters)
      if( known_sites_ref != "") {
        gatk_index_feature_file(
          known_sites_ref,
          gatk_index_feature_file_parameters)
        gatk_get_pileup_summaries(
          norm_bams_bais.concat(tumor_bams_bais),
          gatk_index_feature_file.out.ff_w_index,
          gatk_get_pileup_summaries_parameters)
        gatk_get_pileup_summaries.out.pileups_tables
          .filter{ it[1] =~ /^nd-/ }
          .set{ norm_pileups }
        gatk_get_pileup_summaries.out.pileups_tables
          .filter{ it[1] =~ /^ad-/ }
          .set{ tumor_pileups }
        norm_pileups
          .join(tumor_pileups, by: [0, 2])
          .set{ norm_tumor_pileups }
      gatk_calculate_contamination(
        norm_tumor_pileups,
        '')
      gatk_mutect2_matched.out.vcfs_w_stats
        .join(gatk_calculate_contamination.out.contamination_tables, by: [0, 1, 2, 3])
        .join(gatk_learn_read_orientation_model.out.artifact_priors, by: [0, 1, 2, 3])
        .set{ vcfs_w_stats_w_contam_w_arti_priors }
      } else {
         gatk_mutect2_matched.out.vcfs_w_stats
           .join(gatk_learn_read_orientation_model.out.artifact_priors, by: [0, 1, 2, 3])
           .map{ [it[0], it[1], it[2], it[3], it[4], it[5], params.dummy_file, it[6]] }
           .set{ vcfs_w_stats_w_contam_w_arti_priors }
      }
      gatk_filter_mutect_calls(
        vcfs_w_stats_w_contam_w_arti_priors,
        make_ancillary_index_files.out.collective_idx_files,
        gatk_filter_mutect_calls_parameters,
        gatk_filter_mutect_calls_suffix)
     gatk_filter_mutect_calls.out.filtd_vcfs
       .set{ mutect2_vars }
    }
    if( som_var_caller =~ /abra2|abra2cadabra|cadabra/ ) {
      abra2_parameters = som_var_caller_parameters['abra2'] ? som_var_caller_parameters['abra2'] : ''
      abra2_suffix = som_var_caller_suffix['abra2'] ? som_var_caller_suffix['abra2'] : ''
      abra2_cadabra(
        norm_tumor_bams_bais,
        aln_ref,
        abra2_suffix,
        abra2_parameters)
      abra2_cadabra.out.vcfs
        .set{ abra2_vars }
    }
    Channel.empty()
      .concat(mutect2_vars)
      .concat(strelka2_snv_vars)
      .concat(strelka2_indel_vars)
      .concat(varscan2_snv_vars)
      .concat(varscan2_indel_vars)
      .concat(abra2_vars)
      .set{ som_vars }

  emit:
    som_vars
    mutect2_vars
    strelka2_snv_vars
    strelka2_indel_vars
    varscan2_snv_vars
    varscan2_indel_vars
    abra2_vars
}


workflow som_vars_to_filtd_som_vars {
// require:
//   VCFS
//   params.somatic$som_vars_to_filtd_som_vars$vcf_filtering_tool
//   params.somatic$som_vars_to_filtd_som_vars$vcf_filtering_tool_parameters
  take:
    vcfs
    vcf_filter_tool
    vcf_filter_tool_parameters
  main:
    filtd_som_vars = Channel.empty()
    mutect2_filtd_vars = Channel.empty()
    strelka2_filtd_vars = Channel.empty()
    varscan2_filtd_vars = Channel.empty()
    abra2_filtd_vars = Channel.empty()
    vcf_filter_tool_parameters = Eval.me(vcf_filter_tool_parameters)
    if( vcf_filter_tool =~ /bcftools/ ) {
      bcftools_filter_mutect2_parameters = vcf_filter_tool_parameters['mutect2'] ? vcf_filter_tool_parameters['mutect2'] : '-i \'FILTER="PASS"\''
      bcftools_filter_strelka2_parameters = vcf_filter_tool_parameters['strelka2'] ? vcf_filter_tool_parameters['strelka2'] : '-i \'FILTER="PASS"\''
      bcftools_filter_abra2_parameters = vcf_filter_tool_parameters['abra2'] ? vcf_filter_tool_parameters['abra2'] : '-i \'FILTER="PASS"\''
      bcftools_filter_varscan2_parameters = vcf_filter_tool_parameters['varscan2'] ? vcf_filter_tool_parameters['varscan2'] : '-i \'FILTER="PASS" && INFO/SS="2"\''
      if (vcfs.filter{ it[4] =~ /mutect2/ }) {
        bcftools_filter_mutect2(
          vcfs.filter{ it[4] =~ /mutect2/ },
          bcftools_filter_mutect2_parameters)
        bcftools_filter_mutect2.out.filtd_vcfs
          .set{ mutect2_filtd_vars }
      }
      if (vcfs.filter{ it[4] =~ /strelka2/ }) {
        bcftools_filter_strelka2(
          vcfs.filter{ it[4] =~ /strelka2/ },
          bcftools_filter_strelka2_parameters)
        bcftools_filter_strelka2.out.filtd_vcfs
          .set{ strelka2_filtd_vars }
      }
      if (vcfs.filter{ it[4] =~ /varscan2/ }) {
        bcftools_filter_varscan2(
          vcfs.filter{ it[4] =~ /varscan2/ },
          bcftools_filter_varscan2_parameters)
        bcftools_filter_varscan2.out.filtd_vcfs
          .set{ varscan2_filtd_vars }
      }
      if (vcfs.filter{ it[4] =~ /abra2/ }) {
        bcftools_filter_abra2(
          vcfs.filter{ it[4] =~ /abra2/ },
          bcftools_filter_abra2_parameters)
        bcftools_filter_abra2.out.filtd_vcfs
          .set{ abra2_filtd_vars }
      }
      mutect2_filtd_vars
        .concat(strelka2_filtd_vars)
        .concat(varscan2_filtd_vars)
        .concat(abra2_filtd_vars)
        .set { filtd_som_vars }
    }
  emit:
    filtd_som_vars
    mutect2_filtd_vars
    strelka2_filtd_vars
    abra2_filtd_vars
}

workflow som_vars_to_normd_som_vars {
// require:
//   params.somatic$som_vars_to_normd_som_vars$vcf_norming_tool
//   params.somatic$som_vars_to_normd_som_vars$vcf_norming_tool_parameters
//   params.somatic$som_vars_to_normd_som_vars$aln_ref
  take:
    vcfs
    vcf_norming_tool
    vcf_norming_tool_parameters
    aln_ref
  main:
    samtools_faidx(
      aln_ref,
      '')
    vcf_norming_tool_parameters = Eval.me(vcf_norming_tool_parameters)
    if( vcf_norming_tool =~ /bcftools/ ) {
      bcftools_norm_mutect2_parameters = vcf_norming_tool_parameters['mutect2'] ? vcf_norming_tool_parameters['mutect2'] : ''
      bcftools_norm_strelka2_parameters = vcf_norming_tool_parameters['strelka2'] ? vcf_norming_tool_parameters['strelka2'] : ''
      bcftools_norm_varscan2_parameters = vcf_norming_tool_parameters['varscan2'] ? vcf_norming_tool_parameters['varscan2'] : ''
      bcftools_norm_abra2_parameters = vcf_norming_tool_parameters['abra2'] ? vcf_norming_tool_parameters['abra2'] : ''
      bcftools_norm_mutect2(
        vcfs.filter{ it[4] =~ /mutect2/ },
        samtools_faidx.out.faidx_file,
        bcftools_norm_mutect2_parameters)
      bcftools_norm_stelka2(
        vcfs.filter{ it[4] =~ /strelka2/ },
        samtools_faidx.out.faidx_file,
        bcftools_norm_strelka2_parameters)
      bcftools_norm_abra2(
        vcfs.filter{ it[4] =~ /abra2/ },
        samtools_faidx.out.faidx_file,
        bcftools_norm_abra2_parameters)
      //VarScan2 VCFs do not seem to like being normalized. Going to look into
      //this further. Pass through for now.
      vcfs.filter{ it[4] =~ /varscan2/ }
        .set { varscan2_non_normed }
    }
    // Include individual emissions as well.
    bcftools_norm_mutect2.out.normd_vcfs
      .concat(bcftools_norm_stelka2.out.normd_vcfs)
      .concat(varscan2_non_normed)
      .concat(bcftools_norm_abra2.out.normd_vcfs)
      .set { normd_som_vars }
  emit:
    normd_som_vars = normd_som_vars
}

workflow som_vars_to_isecd_som_vars {
// require:
//   params.somatic$som_vars_to_isecd_som_vars$vcf_isecing_tool
//   params.somatic$som_vars_to_isecd_som_vars$vcf_isecing_tool_parameters
  take:
    som_vars
    vcf_isecing_tool
    vcf_isecing_tool_parameters
  main:
    // Group by sample
    som_vars
      .groupTuple(by: [0, 1, 2, 3])
      .set{ by_patient_som_vars }

    vcf_isecing_tool_parameters = Eval.me(vcf_isecing_tool_parameters)
//    if( vcf_isecing_tool =~ /bcftools/ ) {
//      bcftools_isec_parameters = vcf_isecing_tool_parameters['bcftools_isec'] ? vcf_isecing_tool_parameters['bcftools_isec'] : ''
//    }
    if( vcf_isecing_tool =~ /bedtools/ ) {
      bedtools_intersect_parameters = vcf_isecing_tool_parameters['bedtools_intersect'] ? vcf_isecing_tool_parameters['bedtools_intersect'] : ''
      bedtools_intersect_vcfs(
        by_patient_som_vars,
        bedtools_intersect_parameters)
    }
  emit:
    isecd_som_vars = bedtools_intersect_vcfs.out.intersect_vcfs
}


workflow som_vars_to_union_som_vars {
// require:
//   params.somatic$som_vars_to_union_som_vars$vcf_merging_tool
//   params.somatic$som_vars_to_union_som_vars$vcf_merging_tool_parameters
  take:
    som_vars
    vcf_merging_tool
    vcf_merging_tool_parameters
  main:
    // Group by sample
    som_vars
      .groupTuple(by: [0, 1, 2, 3])
      .set{ by_patient_som_vars }

    vcf_merging_tool_parameters = Eval.me(vcf_merging_tool_parameters)
    if( vcf_merging_tool =~ /jacquard_merge/ ) {
      jacquard_merge_parameters = vcf_merging_tool_parameters['jacquard_merge'] ? vcf_merging_tool_parameters['jacquard_merge'] : ''
      jacquard_merge(
        by_patient_som_vars,
        jacquard_merge_parameters)
    }
  emit:
    union_som_vars = jacquard_merge.out.union_vcfs
}
